package com.oracle.javaee7.samples.batch.api;

/**
 * A class that serves as input record for a simple payroll
 *  processing system
 */
public class PayrollInputRecord {

    private int id;

    private int baseSalary;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBaseSalary() {
        return baseSalary;
    }

    public void setBaseSalary(int baseSalary) {
        this.baseSalary = baseSalary;
    }

    
    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof PayrollInputRecord)) {
            return false;
        }
        PayrollInputRecord other = (PayrollInputRecord) object;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.oracle.javaee7.samples.batch.api.PayrollInputRecord[ id=" + id + " ]";
    }
    
}
