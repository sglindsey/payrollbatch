/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oracle.javaee7.samples.batch.api;

import javax.batch.api.listener.JobListener;
import javax.batch.runtime.context.JobContext;
import javax.batch.runtime.context.StepContext;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Steve
 */
@Dependent
@Named("infoJobListener")
public class InfoJobListener implements JobListener {

    @Inject
    private JobContext jobContext;


    @Override
    public void beforeJob() throws Exception {
        System.out.println("JOB name: " + jobContext.getJobName() + " starting");
        System.out.println("JOB name: " + jobContext.getBatchStatus() );
    }

    @Override
    public void afterJob() throws Exception {
        System.out.println("JOB name: (" + jobContext.getExitStatus() + ") ended");
    }
}
