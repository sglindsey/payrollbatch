package com.oracle.javaee7.samples.batch.api;

import java.io.Serializable;
import java.util.List;
import javax.batch.api.chunk.AbstractItemWriter;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.naming.InitialContext;

@Named("sysoutItemWriter")
public class SysoutItemWriter
        extends AbstractItemWriter {

    @Override
    public void open(Serializable checkpoint) throws Exception {
        super.open(checkpoint);
    }

    public void writeItems(List list) throws Exception {
        System.out.println("Write " + list.size() + " items");
    }

    
}
