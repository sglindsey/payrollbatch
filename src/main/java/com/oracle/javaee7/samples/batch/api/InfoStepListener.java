/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oracle.javaee7.samples.batch.api;

import javax.batch.api.listener.StepListener;
import javax.batch.runtime.Metric;
import javax.batch.runtime.context.StepContext;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Steve
 */
@Dependent
@Named("infoStepListener")
public class InfoStepListener implements StepListener {

    
    @Inject
    private StepContext stepContext;

    @Override
    public void beforeStep() throws Exception {
        System.out.println("Step: " + stepContext.getStepName() + " " + stepContext.getBatchStatus());
    }

    @Override
    public void afterStep() throws Exception {
        Metric[] metrics = stepContext.getMetrics();
        for (Metric metric : metrics) {
            System.out.println("Step: " + metric.getType() + " " + metric.getValue());
        }
        System.out.println("Step: " + stepContext.getStepName() + " " + stepContext.getExitStatus());
    }
}
